const tailwindLine = require('@tailwindcss/line-clamp');
const tailwindText = require('@tailwindcss/typography');
const tailwindForm = require('@tailwindcss/forms');

module.exports = {
  mode: 'jit',
  purge: [
    './public/**/*.html',
    './src/**/*.vue',
    'node_modules/vue-tailwind/dist/*.js',
  ],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {
      opacity: ['disabled'],
      backgroundColor: ['disabled'],
      cursor: ['disabled'],
    },
  },
  plugins: [
    tailwindText,
    tailwindLine,
    tailwindForm,
  ],
};
