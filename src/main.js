import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import Vuelidate from 'vuelidate';
import PortalVue from 'portal-vue';
import VueChatScroll from 'vue-chat-scroll';
import VuePageTitle from 'vue-page-title';
import VueApexCharts from 'vue-apexcharts';
import NProgress from 'vue-nprogress';
import createAuthRefreshInterceptor from 'axios-auth-refresh';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import './assets/css/app.css';

require('./assets/jsConfig/icon');
require('./assets/jsConfig/vue-tailwind');

const nprogress = new NProgress();
Vue.prototype.$axios = axios;
axios.defaults.baseURL = 'http://localhost:8000/';
const refreshAuthLogic = (failedRequest) => axios.post('/auth/jwt/refresh/', { refresh: store.state.refreshToken })
  .then((res) => {
    store.commit('setToken', {
      access: res.data.access,
    });
    store.dispatch('getMyData').then();
    // eslint-disable-next-line no-param-reassign
    failedRequest.response.config.headers.Authorization = `Bearer ${res.data.access}`;
    return Promise.resolve();
  })
  .catch((e) => {
    console.log('Error in vuex refreshToken()');
    console.log(e);
    return Promise.reject();
  });

// Instantiate the interceptor (you can chain it as it returns the axios instance)
createAuthRefreshInterceptor(axios, refreshAuthLogic);

Vue.config.productionTip = false;
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(PortalVue);
Vue.use(Vuelidate);
Vue.use(VueChatScroll);
Vue.use(NProgress);
Vue.use(VueApexCharts);
Vue.component('apexchart', VueApexCharts);
Vue.use(VuePageTitle, {
  suffix: '- Debote | Chat-Based Debate Simulation Against Bot',
});

Vue.mixin({
  methods: {
    toMinuteSecondTimeString(t) {
      const secNum = parseInt(t, 10); // don't forget the second param
      let minutes = Math.floor(secNum / 60);
      let seconds = secNum - (minutes * 60);

      if (minutes < 10) { minutes = `0${minutes}`; }
      if (seconds < 10) { seconds = `0${seconds}`; }
      return `${minutes}m ${seconds}d`;
    },
    greeting() {
      const d = new Date();
      const time = d.getHours();

      if (time >= 2 && time < 10) return 'Selamat Pagi';
      if (time >= 10 && time < 15) return 'Selamat Siang';
      if (time >= 15 && time < 19) return 'Selamat Sore';
      return 'Selamat Malam';
    },
    filterErrorMessage(err) {
      return err.response.data;
    },
    translateDate(date, withTime = false) {
      const result = new Date(date);
      const days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'];
      const months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
      if (withTime) {
        return `${days[result.getDay()]}, ${result.getDate()} ${months[result.getMonth()]} ${result.getFullYear()} Pukul ${(`0${result.getHours()}`).slice(-2)}.${(`0${result.getMinutes()}`).slice(-2)}.${(`0${result.getSeconds()}`).slice(-2)}`;
      }
      return `${days[result.getDay()]}, ${result.getDate()} ${months[result.getMonth()]} ${result.getFullYear()}`;
    },
  },
});

new Vue({
  router,
  store,
  nprogress,
  render: (h) => h(App),
}).$mount('#app');
