import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Auth/Login.vue'),
  },
  {
    path: '/logout',
    name: 'Logout',
    component: () => import('@/views/Auth/Logout.vue'),
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Auth/AdminRegistration.vue'),
  },
  {
    path: '/admin',
    name: 'AdminDashboard',
    // meta: {
    //   requiresLogin: true,
    // },
    component: () => import('@/views/Admin/AdminDashboard.vue'),
  },
  {
    path: '/admin/group',
    name: 'GroupManagement',
    component: () => import('@/views/Admin/GroupManagement/UserGroup.vue'),
  },
  {
    path: '/admin/histories',
    name: 'StudentHistories',
    component: () => import('@/views/Admin/StudentHistory/StudentHistory.vue'),
  },
  {
    path: '/admin/dataset',
    name: 'DatasetManagement',
    component: () => import('@/views/Admin/DatasetManagement/DebateDataset.vue'),
  },
  // Student Pages
  {
    path: '/dashboard',
    name: 'StudentDashboard',
    component: () => import('@/views/Student/StudentDashboard.vue'),
  },
  {
    path: '/debate',
    name: 'DebateRoom',
    component: () => import('@/views/Student/DebateRoom/DebateRoom.vue'),
  },
  {
    path: '/debate/history/:id',
    name: 'StudentHistory',
    component: () => import('@/views/Student/DebateHistory/HistoryDetail.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresLogin)) {
    if (!this.$store.getters.loggedIn) {
      next({ name: 'Login' });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
