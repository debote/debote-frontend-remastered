import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loggedUser: {
      id: 0,
      name: '',
      username: '',
      is_superuser: false,
    },
    accessToken: localStorage.getItem('accessToken') || null,
    refreshToken: localStorage.getItem('refreshToken') || null,
    debateSessionId: localStorage.getItem('debateSessionId') || null,
  }, // Stored variables
  mutations: {
    setLoggedUser(state, user) {
      state.loggedUser = user;
    },
    setToken(state, { access, refresh = null }) {
      state.accessToken = access;
      localStorage.setItem('accessToken', access);
      if (refresh !== null) {
        state.refreshToken = refresh;
        localStorage.setItem('refreshToken', refresh);
      }
      axios.defaults.headers.common.Authorization = `Bearer ${access}`;
    },
    destroyToken(state) {
      state.accessToken = null;
      state.refreshToken = null;
      localStorage.removeItem('accessToken');
      localStorage.removeItem('refreshToken');
    },
    setDebateSessionId(state, id) {
      state.debateSessionId = id;
      localStorage.setItem('debateSessionId', id);
    },
    removeDebateSessionId(state) {
      state.debateSessionId = null;
      localStorage.removeItem('debateSessionId');
    },
  }, // Async function to interact with variable(s) in state
  getters: {
    loggedIn(state) {
      return state.accessToken != null;
    },
  },
  actions: {
    getMyData(context) {
      axios.get('/auth/users/me/')
        .then((me) => {
          const temp = me.data.id;
          axios.get(`/users/${me.data.id}`)
            .then((myData) => {
              console.log(myData.data);
              context.commit('setLoggedUser', {
                id: temp,
                name: `${myData.data.first_name} ${myData.data.last_name}`,
                username: myData.data.username,
                is_superuser: myData.data.is_superuser,
              });
            })
            .catch((e) => {
              console.log(e);
            });
        })
        .catch((e) => {
          console.log(e);
        });
    },
    userLogin(context, userCredentials) {
      return new Promise((resolve, reject) => {
        axios.post('/auth/jwt/create/', userCredentials)
          .then((res) => {
            context.commit('setToken', {
              access: res.data.access,
              refresh: res.data.refresh,
            });
            context.dispatch('getMyData')
              .then(() => resolve())
              .catch(() => reject());
          })
          .catch((e) => {
            console.log('Error in vuex userLogin()');
            console.log(e);
            reject();
          });
      });
    },
    userLogout(context) {
      if (context.getters.loggedIn) context.commit('destroyToken');
    },
    refreshToken(context) {
      return new Promise((resolve, reject) => {
        axios.post('/auth/jwt/refresh/', { refresh: this.state.refreshToken })
          .then((res) => {
            console.log(res.data);
            context.commit('setToken', {
              access: res.data.access,
            });
            if (this.state.loggedUser.id === 0) context.dispatch('getMyData').then();
            resolve();
          })
          .catch((e) => {
            console.log('Error in vuex refreshToken()');
            console.log(e);
            reject();
          });
      });
    },
  },
  modules: {
  },
});
